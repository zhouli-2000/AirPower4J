package cn.hamm.airpower.validate.phone;

import cn.hamm.airpower.util.AirUtil;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.jetbrains.annotations.NotNull;
import org.springframework.util.StringUtils;


/**
 * <h1>电话验证实现类</h1>
 *
 * @author Hamm.cn
 */
public class PhoneAnnotationValidator implements ConstraintValidator<Phone, String> {
    /**
     * 是否座机
     */
    private boolean tel = true;
    /**
     * 是否手机号
     */
    private boolean mobile = true;

    @Override
    public final boolean isValid(String value, ConstraintValidatorContext context) {
        if (!StringUtils.hasLength(value)) {
            return true;
        }
        if (!mobile && !tel) {
            // 不允许座机也不允许手机 验证个鬼啊
            return true;
        }
        if (!mobile) {
            // 只允许座机
            return AirUtil.getValidateUtil().isTelPhone(value);
        }
        if (!tel) {
            // 只允许手机
            return AirUtil.getValidateUtil().isMobilePhone(value);
        }
        // 手机座机均可
        return AirUtil.getValidateUtil().isMobilePhone(value) || AirUtil.getValidateUtil().isTelPhone(value);
    }

    @Override
    public final void initialize(@NotNull Phone constraintAnnotation) {
        mobile = constraintAnnotation.mobile();
        tel = constraintAnnotation.tel();
    }
}